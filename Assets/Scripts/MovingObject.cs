﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {

	private BoxCollider2D boxCollider;
	private Rigidbody2D rigidBody;

	protected virtual void Start () {
		boxCollider = GetComponent<BoxCollider2D> ();
		rigidBody = GetComponent<Rigidbody2D> ();
	}

	protected bool CanObjectMove(int xDirection, int yDriection){
		Vector2 startPosition = rigidBody.position;
		Vector2 endPostition = startPosition + new Vector2 (xDirection, yDriection);

		rigidBody.MovePosition (endPostition);
		//StartCoroutine(SmoothMovementRoutine (endPostition));
		return true;
	}

	protected IEnumerable SmoothMovementRoutine(Vector2 endPosition){
		float remainingDistanceToEndPostiton;

		do{
			remainingDistanceToEndPostiton = (rigidBody.position - endPosition).sqrMagnitude;
			Vector2 updatedPostiton = Vector2.MoveTowards(rigidBody.position, endPosition, 10f * Time.deltaTime);
			rigidBody.MovePosition(updatedPostiton);
			yield return null;
		}
		while(remainingDistanceToEndPostiton > float.Epsilon);
	}

	void Update () {
	
	}
}
